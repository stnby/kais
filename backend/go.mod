module backend

go 1.15

require (
	github.com/lib/pq v1.8.0 // indirect
	github.com/mmcloughlin/geohash v0.10.0 // indirect
	github.com/stnby/go-gpsd v0.2.3-0.20201111161223-6a81d7cc7a60 // indirect
)

package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"github.com/stnby/go-gpsd"
	"github.com/mmcloughlin/geohash"
)

func getenv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}

func main() {
	gps, _ := gpsd.Dial(getenv("GPSD_HOST", "gpsd:2947"))
	gps.Subscribe("AIS", func (r interface{}) {
		report := r.(*gpsd.AISReport)
		if (report.MMSI == 0 || report.Lat == 0 || report.Lon == 0) {
			return
		}
		//fmt.Println("MMSI:", report.MMSI)
		//fmt.Println("Geohash:", geohash.Encode(report.Lat, report.Lon), "\n")
		fmt.Println(report.MMSI, geohash.Encode(report.Lat, report.Lon))
	})

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGTERM, syscall.SIGINT)

	gps.Run()
	<-sig

	gps.Close()
}

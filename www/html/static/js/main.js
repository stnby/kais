var map = L.map('map', {
	zoom: 5,
	center: [55, 25]
});

map.zoomControl.setPosition('bottomright');

// Add layers.
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);
L.tileLayer('https://t1.openseamap.org/seamark/{z}/{x}/{y}.png').addTo(map);

map.addControl(new L.Control.ScaleNautic({
	maxWidth: 200,
	imperial: false,
	nautic: true
}));

// Add Polyline Measure.
L.control.polylineMeasure({
	position:'bottomright',
	unit:'nauticalmiles',
	showBearings:true,
	clearMeasurementsOnStop: false,
	showClearControl: true,
	showUnitControl: true
}).addTo(map);

function updateLayer() {
	var bonds = map.getBounds();
	console.log("Bonds: " + JSON.stringify(bonds));
	var zoom = map.getZoom();
	console.log("Zoom: ", zoom);
	if (zoom > 14) {
		hashes = geohash.bboxes(bonds._southWest.lat, bonds._southWest.lng, bonds._northEast.lat, bonds._northEast.lng, precision = 6);
		console.log("Geohash: " + JSON.stringify(hashes));
	}
}

// Update map on changes.
map.on('zoomend', updateLayer);
map.on('moveend', updateLayer);
